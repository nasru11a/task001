package az.unibank.unitech.impl;

import az.unibank.unitech.config.InvalidStateException;
import az.unibank.unitech.dto.SignUpDto;
import az.unibank.unitech.dto.UserDto;
import az.unibank.unitech.exception.UserAlreadyExistsException;
import az.unibank.unitech.model.Role;
import az.unibank.unitech.model.User;
import az.unibank.unitech.model.enumaration.UserRole;
import az.unibank.unitech.repository.RoleRepository;
import az.unibank.unitech.repository.UserRepository;
import az.unibank.unitech.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private static final Long ID = 1001L;
    private static final String PIN = "2C5GH3A";
    private static final String PASSWORD = "123456";

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    void givenSignUpDtoWhenSignUpThenSave(){
        //Arrange
        final SignUpDto signUpDto = returnSignUpDto();
        User user = createUserEntityObject(signUpDto, returnDefaultRoles(UserRole.USER.name()));

        //Act
        userService.signUp(returnSignUpDto());

        //Assert
        verify(userRepository,times(1)).save(user);
    }

    @Test
    void givenUserPinAlreadyExistsWhenSignUpThenException(){
        //Arrange
        String message = UserAlreadyExistsException.getMESSAGE();
        final SignUpDto signUpDto = returnSignUpDto();
        final User user = createUserEntityObject(signUpDto, returnDefaultRoles(UserRole.USER.name()));
        when(userRepository.findByPin(signUpDto.getPin())).thenReturn(Optional.of(user));

        //Assert
        assertThatThrownBy(() -> userService.signUp(signUpDto))
                .isInstanceOf(InvalidStateException.class)
                .hasMessage(String.format(message, signUpDto.getPin()));
    }

    private UserDto returnUserDto(){
        return UserDto.builder()
                .pin(PIN)
                .password(PASSWORD)
                .build();
    }

    private SignUpDto returnSignUpDto(){
        return SignUpDto.builder()
                .pin(PIN)
                .password(PASSWORD)
                .build();
    }

    private Set<Role> returnDefaultRoles(String rolesString) {
        return roleRepository.findUserRoleByName(rolesString);
    }

    private User createUserEntityObject(SignUpDto userDto, Set<Role> roles) {
        User user = new User();
        user.setPin(userDto.getPin());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(roles);
        return user;
    }

    private User createUserEntityObject(UserDto userDto, Set<Role> roles) {
        User user = new User();
        user.setPin(userDto.getPin());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(roles);
        return user;
    }

}
