package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;

public class NoEnoughAmountInSenderAccountException extends InvalidStateException {

    public static final String MESSAGE = "There is no such amount in %s account.";

    public NoEnoughAmountInSenderAccountException(String senderAccount) {
        super(String.format(MESSAGE, senderAccount));
    }
}
