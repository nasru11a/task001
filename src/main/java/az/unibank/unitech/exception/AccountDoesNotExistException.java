package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;

public class AccountDoesNotExistException extends InvalidStateException {

    public static final String MESSAGE = "%s account does not exist.";
    public AccountDoesNotExistException(String name) {
        super(String.format(MESSAGE, name));
    }
}
