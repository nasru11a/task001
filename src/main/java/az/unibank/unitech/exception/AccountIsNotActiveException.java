package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;

public class AccountIsNotActiveException extends InvalidStateException {

    public static final String MESSAGE = "Account %s is not active.";

    public AccountIsNotActiveException(String receiverAccountName) {
        super(String.format(MESSAGE, receiverAccountName));
    }
}
