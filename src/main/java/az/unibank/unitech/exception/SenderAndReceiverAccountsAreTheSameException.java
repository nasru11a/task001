package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;

public class SenderAndReceiverAccountsAreTheSameException extends InvalidStateException {

    public static final String MESSAGE = "Sender %s and receiver %s accounts are the same.";

    public SenderAndReceiverAccountsAreTheSameException(String senderAccountName, String receiverAccountName) {
        super(String.format(MESSAGE,senderAccountName,receiverAccountName));
    }
}
