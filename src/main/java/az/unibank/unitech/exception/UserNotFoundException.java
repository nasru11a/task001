package az.unibank.unitech.exception;

import az.unibank.unitech.config.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 58432132465811L;

    public UserNotFoundException(String nationalId) {
        super(String.format("User with pin \"%s\" is not found.", nationalId));
    }
}
