package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;

public class PasswordIsIncorrectException extends InvalidStateException {

    public static final String OLD_PASSWORD_NOT_CORRECT = "Password is not correct";

    private static final long serialVersionUID = 39453245432534L;

    public PasswordIsIncorrectException() {
        super(OLD_PASSWORD_NOT_CORRECT);
    }
}
