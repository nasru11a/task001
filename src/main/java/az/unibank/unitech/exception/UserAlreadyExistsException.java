package az.unibank.unitech.exception;

import az.unibank.unitech.config.InvalidStateException;
import lombok.Getter;

public class UserAlreadyExistsException extends InvalidStateException {

    private static final long serialVersionUID = 1L;

    @Getter
    private static final String MESSAGE = "User with pin \"%s\" already exist, please login your account";

    public UserAlreadyExistsException(String nationalId) {
        super(String.format(MESSAGE, nationalId));
    }
}
