package az.unibank.unitech.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
public class AccountToAccountDto {
    @NotBlank(message = "Sender account must not be blank.")
    String sender;
    @NotBlank(message = "Receiver account must not be blank.")
    String receiver;

    Double transferAmount;
}
