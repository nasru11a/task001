package az.unibank.unitech.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CurrencyRateDto {
    @NotBlank(message = "Enter from which currency.")
    String fromCurrency;
    @NotBlank(message = "Enter to which currency.")
    String toCurrency;
}
