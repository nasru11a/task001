package az.unibank.unitech.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignUpDto {
    @NotBlank(message = "PIN must not be blank.")
    @Size(max = 7, min = 7, message = "PIN must contain 7 characters.")
    private String pin;

    @NotBlank(message = "Password must not be blank.")
    @Size(min = 6, message = "Password must contain at least 6 characters.")
    private String password;
}
