package az.unibank.unitech.service.impl;

import az.unibank.unitech.dto.SignUpDto;
import az.unibank.unitech.dto.UserDto;
import az.unibank.unitech.exception.PasswordIsIncorrectException;
import az.unibank.unitech.exception.UserAlreadyExistsException;
import az.unibank.unitech.exception.UserNotFoundException;
import az.unibank.unitech.model.Role;
import az.unibank.unitech.model.User;
import az.unibank.unitech.model.enumaration.UserRole;
import az.unibank.unitech.repository.RoleRepository;
import az.unibank.unitech.repository.UserRepository;
import az.unibank.unitech.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public void signUp(SignUpDto requestDto) {
        errorIfPinAlreadyExist(requestDto.getPin());
        User user = createUserEntityObject(requestDto, returnDefaultRoles(UserRole.USER.name()));
        userRepository.save(user);
    }

    public void signIn(UserDto requestDto){
        log.trace("Login request by user {}", requestDto.getPin());
        User user = userRepository.findByPin(requestDto.getPin())
                .orElseThrow(() -> new UserNotFoundException(requestDto.getPin()));
        matchPassword(requestDto.getPassword(), user.getPassword());
    }

    public void matchPassword(String rawPassword, String encodedPassword){
        boolean isMatched = passwordEncoder.matches(rawPassword, encodedPassword);
        if(!isMatched) throw new PasswordIsIncorrectException();
    }

    private void errorIfPinAlreadyExist(String pin){
        userRepository.findByPin(pin)
                .ifPresent(user -> {
                    throw new UserAlreadyExistsException(pin);
                });
    }

    private User createUserEntityObject(SignUpDto userDto, Set<Role> roles) {
        User user = new User();
        user.setPin(userDto.getPin());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(roles);
        return user;
    }

    private Set<Role> returnDefaultRoles(String rolesString) {
        return roleRepository.findUserRoleByName(rolesString);
    }
}
