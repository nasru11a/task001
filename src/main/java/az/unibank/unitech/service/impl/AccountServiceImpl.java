package az.unibank.unitech.service.impl;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.dto.AccountToAccountDto;
import az.unibank.unitech.dto.CurrencyRateDto;
import az.unibank.unitech.exception.*;
import az.unibank.unitech.model.Account;
import az.unibank.unitech.model.User;
import az.unibank.unitech.repository.AccountRepository;
import az.unibank.unitech.repository.UserRepository;
import az.unibank.unitech.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{

    private final AccountRepository accountRepository;

    private final UserRepository userRepository;

    private final ModelMapper mapper;

    private static final String ACTIVE_STATUS = "ACTIVE";

    @Override
    public List<AccountDto> getUserActiveAccounts(String pin) {
        log.info("Getting user active accounts!");
        User user = userRepository.findByPin(pin).get();
        Long userId = user.getId();
        return accountRepository
                .findAllByStatusAndUserId(ACTIVE_STATUS, userId)
                .stream()
                .map(account -> mapper.map(account, AccountDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void makeTransferAccountToAccount(String pin, AccountToAccountDto accountToAccountDto) {

        Long userId = returnUserIdByPin(pin);
        Account senderAccount = accountRepository.findAccountByAccountNameAndUserId(accountToAccountDto.getSender(), userId)
                .orElseThrow(() -> new AccountDoesNotExistException(accountToAccountDto.getSender()));
        Account receiverAccount = accountRepository.findAccountByAccountNameAndUserId(accountToAccountDto.getReceiver(), userId)
                .orElseThrow(() -> new AccountDoesNotExistException(accountToAccountDto.getReceiver()));
        BigDecimal transferAmount = new BigDecimal(accountToAccountDto.getTransferAmount());

        validateAccountsToMakeSuccessfulTransfer(senderAccount, receiverAccount, transferAmount);

        makeTransferAndSave(senderAccount, receiverAccount, transferAmount);
    }

    @Override
    public HttpResponse<String> getCurrencyRate(CurrencyRateDto currencyRateDto) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(String.format("https://api.fastforex.io/fetch-one?from=%s&to=%s&api_key=1cfbb29b66-877cf6f8a6-r8hb4g",
                        currencyRateDto.getFromCurrency(), currencyRateDto.getToCurrency())))
                .header("Accept", "application/json")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return response;
    }

    private void makeTransferAndSave(Account senderAccount, Account receiverAccount, BigDecimal transferAmount){
        senderAccount.setBalance(BigDecimal.valueOf(senderAccount.getBalance().doubleValue() - transferAmount.doubleValue()));
        receiverAccount.setBalance(BigDecimal.valueOf(receiverAccount.getBalance().doubleValue() + transferAmount.doubleValue()));
        accountRepository.save(mapper.map(senderAccount, Account.class));
        accountRepository.save(mapper.map(receiverAccount, Account.class));
    }

    private Long returnUserIdByPin(String pin) {
        User user = userRepository.findByPin(pin)
                .orElseThrow(() -> new UserNotFoundException(pin));
        return user.getId();
    }

    private void validateAccountsToMakeSuccessfulTransfer(Account senderAccount, Account receiverAccount, BigDecimal transferAmount){
        errorIfSenderAccountIsNotActive(senderAccount.getAccountName(), senderAccount.getStatus());
        errorIfReceiverAccountIsNotActive(receiverAccount.getAccountName(), receiverAccount.getStatus());
        errorIfSenderAndReceiverAccountsAreTheSame(senderAccount.getAccountName(), receiverAccount.getAccountName());
        errorIfNoEnoughAmountInSenderAccount(senderAccount.getAccountName(), senderAccount.getBalance(), transferAmount);
    }

    private void errorIfNoEnoughAmountInSenderAccount(String senderAccountName, BigDecimal senderAccountBalance, BigDecimal transferAmount){
        if(senderAccountBalance.doubleValue() < transferAmount.doubleValue()){
            throw new NoEnoughAmountInSenderAccountException(senderAccountName);
        }
    }

    private void errorIfSenderAndReceiverAccountsAreTheSame(String senderAccountName, String receiverAccountName){
        if(senderAccountName.equals(receiverAccountName)){
            throw new SenderAndReceiverAccountsAreTheSameException(senderAccountName, receiverAccountName);
        }
    }

    private void errorIfReceiverAccountIsNotActive(String receiverAccountName ,String receiverAccountStatus){
        if(receiverAccountStatus.equals("INACTIVE")){
            throw new AccountIsNotActiveException(receiverAccountName);
        }
    }

    private void errorIfSenderAccountIsNotActive(String senderAccountName ,String senderAccountStatus){
        if(senderAccountStatus.equals("INACTIVE")){
            throw new AccountIsNotActiveException(senderAccountName);
        }
    }
}
