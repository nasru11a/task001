package az.unibank.unitech.service;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.dto.AccountToAccountDto;
import az.unibank.unitech.dto.CurrencyRateDto;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.http.HttpResponse;
import java.util.List;

public interface AccountService {
    List<AccountDto> getUserActiveAccounts(String pin);

    void makeTransferAccountToAccount(String pin, AccountToAccountDto accountToAccountDto);

    HttpResponse<String> getCurrencyRate(CurrencyRateDto currencyRateDto);
}
