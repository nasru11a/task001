package az.unibank.unitech.service;

import az.unibank.unitech.dto.SignUpDto;
import az.unibank.unitech.dto.UserDto;

public interface UserService {
    void signUp(SignUpDto userDto);
    void signIn(UserDto requestDto);
}
