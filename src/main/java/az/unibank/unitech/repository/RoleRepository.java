package az.unibank.unitech.repository;

import az.unibank.unitech.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, String> {

    Set<Role> findUserRoleByName(String name);
}
