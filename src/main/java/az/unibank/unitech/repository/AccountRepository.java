package az.unibank.unitech.repository;

import az.unibank.unitech.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findAllByStatusAndUserId(String status, Long userId);
    Optional<Account> findAccountByAccountNameAndUserId(String accountName, Long userId);
}
