package az.unibank.unitech.security;

import az.unibank.unitech.exception.UserNotFoundException;
import az.unibank.unitech.model.User;
import az.unibank.unitech.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UniTechUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String pin) throws UserNotFoundException {
        return userRepository.findByPin(pin)
                .map(this::createSpringSecurityUser)
                .orElseThrow(() ->
                        new UserNotFoundException(pin));
    }

    private CustomSpringSecurityUser createSpringSecurityUser(User user) {
        List<GrantedAuthority> grantedAuthorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
        return new CustomSpringSecurityUser(
                user.getPin(),
                user.getPassword(),
                grantedAuthorities
        );
    }


}
