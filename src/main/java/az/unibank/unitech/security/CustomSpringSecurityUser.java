package az.unibank.unitech.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomSpringSecurityUser extends User {

    private static final long serialVersionUID = 3522416053866116034L;

    public CustomSpringSecurityUser(String nationalId, String password,
                                    Collection<? extends GrantedAuthority> roles) {
        super(nationalId, password, roles);
    }
}
