package az.unibank.unitech.contorller;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.dto.AccountToAccountDto;
import az.unibank.unitech.dto.CurrencyRateDto;
import az.unibank.unitech.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Request;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/accounts")
    public ResponseEntity<List<AccountDto>> getUserActiveAccountsByStatus(Principal principal){
        return new ResponseEntity<>(accountService.getUserActiveAccounts(principal.getName()),
                HttpStatus.OK);
    }

    @PutMapping("/transfer")
    public void makeTransferAccountToAccount(Principal principal, @RequestBody @Valid AccountToAccountDto accountToAccountDto){
        accountService.makeTransferAccountToAccount(principal.getName(),
                accountToAccountDto);
    }

    @GetMapping("/convert-currency")
    public ResponseEntity<String> convertCurrency(@RequestBody @Valid CurrencyRateDto currencyRateDto){
        return new ResponseEntity<>(accountService.getCurrencyRate(currencyRateDto).body(),
                HttpStatus.OK);
    }
}
