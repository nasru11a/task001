package az.unibank.unitech.contorller;

import az.unibank.unitech.dto.SignUpDto;
import az.unibank.unitech.dto.UserDto;
import az.unibank.unitech.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class UserController {

    private final UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Valid SignUpDto requestDto) {
        log.trace("Sign up request with email {}", requestDto.getPin());
        userService.signUp(requestDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/sign-in")
    public ResponseEntity<Void> signIn(@RequestBody @Valid UserDto userDto){
        log.trace("Login request by user {}", userDto.getPin());
        userService.signIn(userDto);
        log.trace("Authenticating user {}", userDto.getPin());
        return ResponseEntity.ok().build();
    }
}

