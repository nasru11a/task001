package az.unibank.unitech.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserMapper {

    @Bean
    public org.modelmapper.ModelMapper getModelMapper(){
        return new org.modelmapper.ModelMapper();
    }

}
