package az.unibank.unitech;

import az.unibank.unitech.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class UniTechApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(UniTechApplication.class, args);
	}

	private final AccountRepository accountRepository;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Testing JPA");
		accountRepository.findAllByStatusAndUserId("ACTIVE", 1l);
		accountRepository.findAccountByAccountNameAndUserId("ADMIN001", 1l);
	}
}
