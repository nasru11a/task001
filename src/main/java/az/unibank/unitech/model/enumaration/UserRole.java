package az.unibank.unitech.model.enumaration;

public enum UserRole {
    USER, ADMIN, SUPER_USER
}
